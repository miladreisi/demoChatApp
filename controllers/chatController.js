var express = require('express');
var path = require('path')
var io;
exports.start = function(ioP)
{
    io = ioP;
    io.on('connection',function(socket){
        userConnected();
        socket.on('disconnect',function(){
          userDisconnected();
        })

        socket.on('message',function(data)
        {
            onMessage(data);
        })
    });
};

exports.index = function(req,res)
{
    //res.send('ok2')
    res.sendFile('index.html',{
        root: path.join(__dirname,'../public/chat')
    })
}

function userConnected()
{
    console.log('user connected')
}
function userDisconnected()
{
    console.log('user disconnected')
}
function onMessage(data)
{
    io.emit('newMessage',data);
}