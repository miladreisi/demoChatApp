import React, { Component } from 'react';
import './App.css';
import $ from 'jquery'
import socketIOClient from 'socket.io-client'
class App extends Component {

    constructor()
    {
        super()
        this.state = {
            server: 'localhost:',
            username: '',
            message: ''
        }

        this.processNewMessage = this.processNewMessage.bind(this)
    }
    render() {
    return (
        <div>
            <input type='text' id='usernameTxt' placeholder='enter your username...'></input>
            <br/>
            <textarea id='messageTxt'></textarea>
            <br/>
            <button id='sendBtn'>send</button>

            <div id='chats'></div>
        </div>
    );
  }

  componentDidMount()
  {
      const endpoint = this.state.server
      const socket = socketIOClient(endpoint)
      socket.on('newMessage',this.processNewMessage)
      $('#sendBtn').click(function(){
          socket.emit('message',{
              username:$('#usernameTxt').value,
              message: $('#messageTxt').value
          })
          $('#messageTxt').text('')
      })
  }

  processNewMessage(message)
  {
      console.log(message)
  }

}

export default App;
