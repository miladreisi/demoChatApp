var socket = io();
socket.on('connect',function(){
    console.log('socket connected');

    socket.on('newMessage',function(data){
        //console.log(data);
        document.getElementById('chatarea').innerHTML += data.username + ': ' + data.message + '<br/>';
    });
});
function sendMessageEventHandler()
{
    var username = document.getElementById('nameTxt');
    var message = document.getElementById('messageTxt');
    socket.emit('message',{
        username: username.value,
        message: message.value
    });

    //username.value = '';
    message.value = '';
}
window.onload = function()
{
    console.log('document loaded');
    init()
};

function init()
{
    var sendBtn = document.getElementById('sendBtn');
    sendBtn.addEventListener('click',sendMessageEventHandler);
}
function processNewMessage(newMessage)
{
    console.log(newMessage);
}